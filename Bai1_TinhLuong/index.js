/*
Bài 1: Tính lương nhân viên
Lương 1 ngày: 100.000
Số ngày làm
Công thức = lương 1 ngày * số ngày làm
*/

const luong1Ngay = 100000
var soNgayLam = 22

var luongThang = luong1Ngay * soNgayLam
console.log("Lương 1 ngày =", luong1Ngay);
console.log("Số ngày làm =", soNgayLam);
console.log("Lương tháng =", luongThang);